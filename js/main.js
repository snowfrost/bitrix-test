$(function(){


});


function result()
{
    var bad = 0
    $('input').each(function(){
        $(this).parent().removeClass('bg-danger');
        if ($(this).is(':checked'))
        {
            if($(this).attr('class') == 'test_item_ok')
            {
                $(this).parent().addClass('bg-success');
            }
            else
            {
                $(this).parent().addClass('bg-danger');
                bad++;
            }

        } else {
            if($(this).attr('class') == 'test_item_ok')
            {
                $(this).parent().addClass('bg-danger');
                bad++;
            }

        }

    });

    if (bad == 0)
    {
        $('#result').html('<p class="bg-success">Тест сдан</p>');
    }
    else
    {
        $('#result').html('<p class="bg-danger">Тест не сдан</p>');
    }

    console.log(bad);

}
function reset()
{
    $('label').removeClass('bg-success');
    $('label').removeClass('bg-danger');
    $('input').prop('checked', false);
    $('#result').html('');

}
