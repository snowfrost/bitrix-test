<?php
	include_once 'include/simplehtmldom_1_5/simple_html_dom.php';

	class Test {
		public $html = false;
		public $content =false;
		public $dom=false;
		public $address;
		public $questions;
		public $testcontent;
		public $filename;
		public $headName;

		public function __construct($address)
		{
			$this->address = $address;
			$this->makeName();
			if (file_exists($this->filename)){
				$this->html = file_get_html($this->filename);
			}
			else{
				$file = file_get_contents($address);
				file_put_contents($this->filename, $file);
				$this->html = file_get_html($this->filename);
			}

			$this->setContent();
			$this->setQuestions();
			$this->makeFile();
			$this->fileSave();

		}

		public function setContent(){
			$this->content = $this->html->find('#test ol', 0);
			$this->headName = $this->html->find('h1',0)->plaintext;
		}

		public function setQuestions(){

			foreach ($this->content->children() as $el)
			{
				$temp = '';
				foreach ($el->find('li') as $li)
				{
					$a['CLASS'] = $li->class;
					$a['TEXT'] =str_replace('- ', '' , $li->plaintext) ;

					$temp[] = $a;
					unset ($a);
				}

				$this->questions[] = array(
					'NAME' => $el->find('h4',0)->plaintext,
					'VARIANTS' => $temp
				);
			}
		}

		public function printContent()
		{
			print_r($this->questions);
		}

		public function getQuestions()
		{
			return $this->questions;
		}

		public function makeFile()
		{
			$this->testcontent = '<h1>'.$this->headName.'</h1>';
			$this->testcontent .= '<div>'.PHP_EOL;
			foreach ($this->questions as $question)
			{
				$this->testcontent .='<div>';
				$this->testcontent .= '<h4 class="bg-primary">'. $question['NAME'] .'</h4>'.PHP_EOL;
				foreach ($question['VARIANTS'] as $var)
				{
					$this->testcontent.= '<label class="'.$var['CLASS'].'"><input type="checkbox" class="' . $var['CLASS'] . '" value=""/>';
					$this->testcontent.= $var['TEXT'];
					$this->testcontent.= '</label>'.PHP_EOL;
				}
				$this->testcontent.='</div>';
			}
			$this->testcontent .='</div>'.PHP_EOL;
		}

		public function fileSave()
		{
			file_put_contents('bitrix-tests/' . $this->filename.'-test.php', $this->testcontent);

		}

		public function makeName(){
			$t = parse_url($this->address);
			$t = explode('/', $t['path']);
			$t = explode('.', $t[3]);
			$this->filename = $t[0];
		}
	}

	class TestDriveTest
	{
		private $class;

		public function __construct($class)
		{
			$this->class = $class;
			$this->testMethods();
		}

		public function res($result)
		{
			echo ($result)?  '+':  '-';
		}

		public function testMethods()
		{
			$this->res(is_object($this->class->html));
			$this->res(is_object($this->class->content));
			$this->res(is_array($this->class->questions));
			$this->res(is_array($this->class->getQuestions()));
		}

	}
	/*$addresses = array(
		//'http://www.sethit.ru/content/bxtests/adm-mod-1.php',
		//'http://www.sethit.ru/content/bxtests/adm-mod-2.php',
		//'http://www.sethit.ru/content/bxtests/adm-mod-3.php',
		//'http://www.sethit.ru/content/bxtests/adm-biz-1.php',
		//'http://www.sethit.ru/content/bxtests/adm-biz-2.php'
	);
	$testphp = '<ul>';
	foreach ($addresses as $addr)
	{
		$B = new Test($addr);
		$testphp .='<li>';
		$testphp .='<a href="/'. $B->filename .'-test.php" >';
		$testphp .=$B->headName;
		$testphp .='<a>';
		$testphp .='</li>';
		unset ($B);
	}
	$testphp .= '</ul>';
	file_put_contents('test.php', $testphp);*/
