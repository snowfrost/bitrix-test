<?php



class HtmlHelper {

	public static function makeQuestion($arr)
	{


		$htmlString = '<div>';
		shuffle($arr);
		foreach ($arr as $element)
		{
			$temp =array();
			$htmlString .='<div>';
			$htmlString .='<h4 class="bg-primary">' . $element['QUESTION'] . '</h4>';

			foreach ($element['ANSWER'] as $key => $value)
			{
				$res = ($value) ? 'test_item_ok': 'test_item_not';
				$temp[] = '<label class="' . $res . '"><input type="checkbox" class="' .$res .'" value=""/>' . $key.'</label>';
				//$htmlString .= '<label class="' . $res . '"><input type="checkbox" class="' .$res .'" value=""/>' . $key.'</label>';
			}

			shuffle($temp);
			$htmlString .= implode('', $temp);

			$htmlString .='</div>';

		}
		$htmlString .= '</div>';

		return $htmlString;
	}

	public static function genSiteMap($arrMap)
	{
		$htmlString = '<ul>';

		foreach ($arrMap as $key => $value)
		{
			$htmlString .= '<li><a href="' . $key .'">' . $value. '</a></li>';
		}


		$htmlString .= '</ul>';
		return $htmlString;
	}

} 