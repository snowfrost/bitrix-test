<?php

	class FileHandler
	{
		private $handle;
		private $arrQuestions;
		private $question = array();


		public function __construct($fileName)
		{
			$handle = fopen($fileName, "r");
			$this->fileHandler($handle);
		}

		public function fileHandler($handle)
		{
			if ($handle) {
				$i = 0;

				while (($buffer = fgets($handle)) !== false)
				{
					if (($buffer != "\n") && ($i == 0))
					{
						if (!empty($this->question['QUESTION']))
						{
							$this->question['QUESTION'] .= trim(htmlspecialchars($buffer)) . "</br>";
						} else {

							$this->question['QUESTION'] = trim(htmlspecialchars( $buffer)) . "</br>";
						}

					}
					elseif (($buffer == "\n") && ($i == 0))
					{
						$i++;
					}
					elseif (($buffer !="\n") && ($i == 1))
					{
						if ((stripos($buffer, 'Выберите ответ') === false))
						{

							$this->question['ANSWER'][str_replace('+', '' , trim(htmlspecialchars($buffer)))] = (stripos($buffer, '+') !== false )? 1: 0 ;
						}
					}
					elseif (($buffer == "\n") && ($i == 1))
					{
						$i = 0;
						$this->nextQuestion();
					}


				}
				if (!feof($handle)) {
					echo "Error: unexpected fgets() fail\n";
				}
				fclose($handle);
			}
		}

		public function nextQuestion()
		{
			$this->arrQuestions[] = $this->question;
			$this->question = array();
		}

		public function getQuestions()
		{
			return $this->arrQuestions;
		}


	}

 