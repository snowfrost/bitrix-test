<?php

	Class Router {
		private $url;
		private $pageName = 'test.php';
		private $sitemap = array(
			"/adm-mod-1-test"=>"Администратор. Модули. Коллективная работа.",
			"/adm-mod-3-test"=>"Администратор. Модули. Служебные модули",
			"/adm-biz-2-test"=>"Администратор. Бизнес. Интернет-магазин",
			"/adm-biz-1-test"=>"Администратор. Бизнес. Валюты и Торговый каталог",
			"/dev-1-test"=>"Разработчик Bitrix Framework. Архитектура продукта",
			"/dev-2-test"=>"Разработчик Bitrix Framework.Технологии",
			"/dev-3-test"=>"Разработчик Bitrix Framework. Интеграция дизайна. Часть 1",
			"/dev-4-test"=>"Разработчик Bitrix Framework. Интеграция дизайна. Часть 2",
			"/dev-5-test"=>"Разработчик Bitrix Framework. Инфоблоки",
			"/dev-6-test"=>"Разработчик Bitrix Framework. Компоненты. Часть 1",
		);
		public $content = false;

		public function run()
		{
			$this->url = $_SERVER['REQUEST_URI'];



			if ($this->url != '/'){
				/*$t = parse_url($this->url);
				$a = explode('/',$t['path']);*/


				$this->getPage($this->url);

			}



			$this->render();
		}

		public function getContent($path)
		{
			return file_get_contents($path);
		}


		public function getPage($pageName)
		{

			$fullPath = 'bitrix-tests' . $pageName . '.php';
			if (file_exists($fullPath))
			{
				$this->content = $this->getContent($fullPath) ;
			}
			else
			{
				$srcPath = 'test-src' . $pageName;
				if (file_exists($srcPath))
				{
					$file = new FileHandler($srcPath);
					$html ='<h1>' . $this->sitemap[$pageName] . '</h1>';
					$html .= HtmlHelper::makeQuestion($file->getQuestions());
					$arr = array($this->sitemap[$pageName] => $file->getQuestions());

					$t = json_encode($arr);

					file_put_contents($fullPath . 1 , $t);

					$this->content = $html;//$this->getContent($fullPath);


				}


			}
		}


		public function render()
		{
			require_once 'header.php';
			if ($this->content)
			{
				echo $this->content;
			}
			else
			{
				echo HtmlHelper::genSiteMap($this->sitemap);
			}

			require_once 'footer.php';
		}


	}

 