<h1>Администратор. Модули. Служебные модули</h1><div>
<div><h4 class="bg-primary">Для защиты веб-проекта на высоком уровне безопасности необходимо:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> настроить стандартный уровень безопасности, а затем выполнить настройку параметров для высокого уровня</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настроить только параметры высокого уровня безопасности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настроить стандартный уровень безопасности, а затем настроить параметры использования одноразовых паролей и контроля активности</label>
</div><div><h4 class="bg-primary">При master-slave репликации в модуле "Веб-кластер"</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> чтение данных будет происходить из основной (master) базы данных, а запись в дополнительные (slave)</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> чтение данных будет происходить из дополнительных (slave) баз данных, а запись в основную (master)</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> чтение данных и запись данных будет происходить как в основную (master) базу, так и в дополнительные (slave)</label>
</div><div><h4 class="bg-primary">Проактивный фильтр не работает для групп пользователей:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> если разрешено использования одноразовых паролей</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> для которых в правах доступа к модулю «Проактивная защита» разрешена операция «Обход проактивного фильтра»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> если включен механизм хранения данных сессий пользователей в базе данных</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> добавленных в стоп-лист</label>
</div><div><h4 class="bg-primary">Сразу после создания подключения к облачному хранилищу</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> старые файлы останутся на хостинге, а новые будут автоматически сохраняться в «облаке»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> старые файлы автоматически будут перенесены в «облако», а новые сразу будут сохраняться там</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на хостинге будут размещаться как старые файлы, так и вновь создаваемые</label>
</div><div><h4 class="bg-primary">Просмотреть содержимое подключенных контейнеров облачных хранилищ можно на странице</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Облачные хранилища (Настройки > Облачные хранилища)</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Облачные хранилища (Контент > Облачные хранилища)</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Управление структурой (Контент > Структура сайта > Файлы и папки)</label>
</div><div><h4 class="bg-primary">Где указывается сервер, на котором размещен почтовый ящик?</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в любом из перечисленных</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля "Почта"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках главного модуля</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках почтового ящика</label>
</div><div><h4 class="bg-primary">Чтобы защита осуществлялась на повышенном уровне необходимо:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настроить параметры повышенного уровня: систему одноразовых паролей и механизм контроля целостности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настроить защиту на стандартном и высоком уровне</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> настроить защиту на стандартном и высоком уровне, а затем настроить параметры повышенного уровня</label>
</div><div><h4 class="bg-primary">Импорт пользователей из AD/LDAP осуществляется:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для всех серверов</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> автоматически</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> из выбранного администратором сервера (вручную)</label>
</div><div><h4 class="bg-primary">Чтобы правило применялось к почтовым сообщениям автоматически, в поле «Применять при событиях» (форма редактирования правила, модуль «Почта») следует выбрать значение:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при получении</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> при принудительном вызове</label>
</div><div><h4 class="bg-primary">Если стандартный уровень не настроен полностью, то:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> защита сайта будет осуществляться на начальном уровне, без учета настроенных параметров на стандартном, высоком и повышенном уровнях</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> защита сайта будет осуществляться на базовом уровне</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> защита сайта будет осуществляться на начальном уровне, но с учетом настроенных параметров на стандартном, высоком и повышенном уровнях</label>
</div><div><h4 class="bg-primary">Если пользователь удален из списка пользователей корпоративной сети, то:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> он сохранит прежний доступ к сайту.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при попытке получить доступ к ресурсам сайта он получит отказ в авторизации.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> его аккаунт будет сохранен в системе управления сайтом.</label>
</div><div><h4 class="bg-primary">Изменить место хранения кеша при установленном модуле веб-кластер можно</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках главного модуля</label>
<label class="test_item_not"><input type="checkbox" class="test_item_ok" value=""/> в административной части сайта на странице "Memcached"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля веб-кластер</label>
<label class="test_item_not"><input type="checkbox" class="test_item_ok" value=""/> в файле "\bitrix\modules\cluster\memcache.php"</label>
</div><div><h4 class="bg-primary">После подключения сервера memcached с помощью модуля веб-кластер</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сервер будет использоваться в кластере совместно с файлами кеша</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> сервер будет использоваться в кластере вместо файлового кеша</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в случае отказа всех memcached-серверов подсистема кеширования снова начинает использовать файловый кеш</label>
</div><div><h4 class="bg-primary">При входе на подключенный к контроллеру сайт через логин на контроллере</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Происходит прозрачная авторизация без создания локального пользователя</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> На сайте создается локальный пользователь с соответствующим именем и правами</label>
</div><div><h4 class="bg-primary">Передача файлов через контроллер сайтов:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> возможна на отдельный сайт или и на группу при условии совпадении имен директорий для загрузки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> невозможна</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> возможна только на отдельный сайт</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> возможна как на отдельный сайт, так и на группу</label>
</div><div><h4 class="bg-primary">Если сведения о группах пользователей корпоративной сети хранятся в базах данных нескольких серверов или в нескольких базах данных одного сервера, то следует:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Создать универсальную запись и указать все возможные сервера и базы данных.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Создать несколько записей, регламентирующих доступ к ним.</label>
</div><div><h4 class="bg-primary">Администратор может выполнять следующие действия с почтовыми ящиками:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> создать новый почтовый ящик</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> просмотреть или добавить новое правило для почтового ящика</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> изменить параметры учетной записи</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> просмотреть все сообщения, пришедшие на этот ящик</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> удалить учетную запись</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> посмотреть журнал событий по данному ящику</label>
</div><div><h4 class="bg-primary">Если перед удалением сайта из контроллера он был предварительно отсоединен, то:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> запись будет удалена, а сам сайт будет работать с теми настройками, которые были установлены контроллером.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> после удаления на сайте будут возвращены настройки, установленные до подключения к контроллеру. </label>
</div><div><h4 class="bg-primary">При подключении сайта к контроллеру</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> На подключенном сайте создается локальный пользователь-администратор, пароль от которого в зашифрованном виде хранится на контроллере</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> На подключенном сайте хранится зашифрованный пароль администратора контроллера</label>
</div><div><h4 class="bg-primary">Страница "Веб-сервера"</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> носит как информационный характер, так и позволяет добавить в кластер новые узлы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> носит информационный характер</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> позволяет добавить в кластер новые узлы</label>
</div><div><h4 class="bg-primary">Модуль AD/LDAP интеграция служит для:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> организации совместной работы корпоративной сети и сайта</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> автоматического доступа в соответствии с уровнем прав</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> организации централизованного управления всеми группами пользователей корпоративной информационной системы</label>
</div><div><h4 class="bg-primary">Проверка целостности файлов системы осуществляется на странице:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Агенты</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Проверка сайта</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Контроль целостности</label>
</div><div><h4 class="bg-primary">Задание структуры компании в AD производится:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> автоматически с помощью специальных свойств пользователя</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> вручную</label>
</div><div><h4 class="bg-primary">Редактирование подключения к облачному хранилищу выполняется</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на странице настроек модуля "Облачные хранилища"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на странице "Облачные хранилища" (Контент > Облачные хранилища)</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> на странице "Облачные хранилища" (Настройки > Облачные хранилища)</label>
</div><div><h4 class="bg-primary">Что отображено на странице Панель безопасности:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> таблица параметров и их значений</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> текущий уровень безопасности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> состояние параметров защиты</label>
</div><div><h4 class="bg-primary">Администратор контроллера на подчиненных сайтах может:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> авторизовываться и выполнять действия в соответствии с правами, заданными локальными администраторами.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> авторизовываться и выполнить необходимые действия без учета уровня прав, установленных локальными администраторами.</label>
</div><div><h4 class="bg-primary">Если при редактировании подключения к облачному хранилищу на закладке "Правила" в колонке "Список модулей" оставить поле пустым</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> то под действие правила не будут попадать файлы ни одного из модулей</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> то под действие правила подпадают файлы любых модулей</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> то под действие правила подпадают файлы только главного модуля</label>
</div><div><h4 class="bg-primary">Контроллер сайтов – это</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> надстройка над Active Directory для управления сайтами, разработанная компанией "1С-Битрикс"</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> модуль Bitrix Framework, который позволяет устанавливать связь с другими независимыми веб-сайтами и централизованно управлять ими</label>
</div><div><h4 class="bg-primary">При деактивации подключения к облачному хранилищу</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> данные будут не доступны для чтения, запись будет невозможна</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> данные будут доступны для чтения, запись будет невозможна</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> данные будут удалены, запись будет невозможна</label>
</div><div><h4 class="bg-primary">Модуль "Веб-Кластер" поддерживает</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> распределение однотипных данных веб-приложения (например, учетных записей) между отдельными базами данных</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> разделение одной базы данных веб-приложения на две и более базы данных за счет выделения отдельных модулей, без изменения логики работы веб-приложения</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> как разделение одной базы данных веб-приложения на две и более базы данных, так и распределение однотипных данных веб-приложения между отдельными базами данных</label>
</div><div><h4 class="bg-primary">Войти на сайт, подключенный к контроллеру, может:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> локальный пользователь этого сайта</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> пользователи контроллера, группе которых разрешен вход на подключенный сайт</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> администратор контроллера</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> все пользователи контроллера</label>
</div><div><h4 class="bg-primary">Если подключенный сайт отмечен в контроллере как неактивный, то:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> он недоступен для любых посетителей.</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> управление этим сайтом со стороны контроллера невозможно.</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на управляемом сайте не выводится информация с управляющего сайта.</label>
</div><div><h4 class="bg-primary">Для того чтобы выполнять получение почтовых сообщений вручную, на странице настройки почтового ящика в поле «Проверять с периодом» следует указать:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> период времени (в минутах), по истечении которого пользователь будет выполнять проверку новых сообщений на сервере</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> 0</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> -1</label>
</div></div>
