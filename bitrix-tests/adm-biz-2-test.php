<h1>Администратор. Бизнес. Интернет-магазин</h1>
<div>
	<div>
		<h4 class="bg-primary"> Экспорт товаров с учетом группировки цен в зависимости от количества приобретаемого товара, а также с учетом валюты, указанной для каждой цены товара, осуществляется в формате: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Yandex</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> CSV</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> CSV (new)</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Froogle</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Yandex-simple</label>
	</div>
	<div><h4 class="bg-primary"> Модуль "Интернет-магазин" не сможет функционировать если: </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в системе не задана хотя бы одна валюта</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в системе не установлен модуль "Валюты", с помощью которого производится настройка валют</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в системе не установлен модуль "Торговый каталог"</label>
	</div>
	<div><h4 class="bg-primary"> Что необходимо сделать, чтобы освободить пользователя от налогов? </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> создать группу пользователей освобожденных от налога, перейти на страницу "Список налогов" и удалить те налоги, которые не должны взиматься</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> создать группу пользователей освобожденных от налога, перейти на страницу "Ставки налогов" и изменить ставки тех налогов, которые не должны взиматься</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> создать группу пользователей освобожденных от налога, перейти на страницу "Освобождение от налогов" и изменить пункт освобождение от налогов, указав какие налоги не должны взиматься</label>
	</div>
	<div>
		<h4 class="bg-primary"> При импорте CSV система посчитает за один и тот же товар те позиции в файле в которых:</h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> совпадают параметры поля Наименование</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> совпадают оба параметра</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> совпадают параметры поля Уникальный идентификатор</label>
	</div>
	<div><h4 class="bg-primary"> Какое количество аффилиатстких пирамид может быть настроено в продукте? </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> 1</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> В соответствии с числом сайтов</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Возможное количество пирамид определяется настройками модуля Интернет-магазин</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Любое</label>
	</div>
	<div><h4 class="bg-primary"> Для корректного функционирования модуля Интернет-магазин необходимо </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> наличие валюты с номиналом в 1 единицу</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> хотя бы одна установленная валюта</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> наличие базовой валюты</label>
	</div>
	<div><h4 class="bg-primary"> В поле "Валюта сумм" формы "Параметры пластиковой карты" указывается: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> все валюты сумм, доступные для операций с картой</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> параметры валют сумм пластиковой карты</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> валюты минимальной и максимальной сумм, которые можно снять с карты</label>
	</div>
	<div>
		<h4 class="bg-primary"> Если одновременно указана ставка НДС для товара и ставка для всего каталога, то для данного товара будет действовать: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> по выбору пользователя, заданному в настройках НДС для товара</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> персональная ставка НДС</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> ставка НДС для всего каталога</label>
	</div>
	<div><h4 class="bg-primary"> Ограничение на применении скидки позволяет задать: </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> группы пользователей, которые могут воспользоваться скидкой</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> разделы, к которым применима скидка</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> товары, на которые дается скидка</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> типы цен, к которым применима скидка</label>
	</div>
	<div>
		<h4 class="bg-primary"> Если при загрузке местоположения загружаемые страна и город уже присутствуют в системе, то:</h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> страна повторно загружаться не будет, а город загружается всегда вне зависимости от присутствия подобных названий в системе</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> страна и город будут загружены повторно</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> страна и город не будут загружены повторно, а будет использована существующая запись</label>
	</div>
	<div><h4 class="bg-primary"> Тип скидки может задаваться: </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> как фиксированная цена</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в процентах</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> как фиксированная сумма</label>
	</div>
	<div><h4 class="bg-primary"> Какие статусы заказа невозможно удалить? </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> N (Принят)</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> F (Выполнен)</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> O (В обработке)</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> D (Отгружен)</label>
	</div>
	<div>
		<h4 class="bg-primary"> Каким образом при изменении наценки автоматически обновить цены всех товаров, к которым она применена? </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Это можно сделать только вручную.</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Это произойдет если при сохранении новых параметров наценки отметить опцию Пересчитать цены.</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Пересчет цен произойдет автоматически, если это разрешено в настройках модуля Торговый каталог.</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Автоматическое обновление цен невозможно.</label>
	</div>
	<div><h4 class="bg-primary"> Скидки могут применяться: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в зависимости от настроек модуля "Торговый каталог"</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> ко всем товарам каталога</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> к одному товару торгового каталога</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> ко всем товарам отдельного раздела каталога</label>
	</div>
	<div><h4 class="bg-primary"> Ставка НДС может быть задана для: </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> товара</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> каталога товаров</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> типа цен</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> группы пользователей</label>
	</div>
	<div><h4 class="bg-primary"> К Типу плательщика привязываются: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Местоположения</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Платежные системы</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Налоги</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Аффилиаты</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Свойства заказа</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Заказы</label>
	</div>
	<div><h4 class="bg-primary"> Платежные системы (отметьте верные утверждения): </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> могут быть настроены под конкретный тип плательщика индивидуально</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_not" value=""/> определенного типа могут работать только с типом плательщика "Юридическое лицо"</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> могут быть отключены для определенного типа плательщика</label>
	</div>
	<div><h4 class="bg-primary"> Обязательные для добавления данные для работы интернет-магазина: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> налоги</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> типы плательщиков;</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> статусы заказов;</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> местоположения;</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> платежные системы;</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> свойства товаров;</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настройка групп местоположений;</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> службы доставки;</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> скидки на заказ</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> группы свойств товаров;</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> аффилиаты</label>
	</div>
	<div><h4 class="bg-primary"> Например, аффилиат "B" зарегистрировался в системе через аффилиата "A", тогда: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> аффилиат "A" будет получать прибыль только за свои продажи</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> аффилиат "B" будет получать прибыль только за свои продажи</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> аффилиат "B" будет получать прибыль не только за свои продажи, но и за продажи аффилиата "A"</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> аффилиат "A" будет получать прибыль не только за свои продажи, но и за продажи аффилиата "B"</label>
	</div>
	<div>
		<h4 class="bg-primary"> Если у типа плательщика не будет задано свойство с флагом "Использовать как местоположение для налогов ", то: </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> этот тип плательщика будет освобожденным от налогов.</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> этот тип плательщика не сможет осуществлять покупки в магазине.</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> у этого типа плательщика ставки налогов определены не будут.</label>
	</div>
	<div><h4 class="bg-primary"> Может ли базовая цена использоваться в качестве оптовой? </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> нет</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> да</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> может, если это разрешено настройками модуля Торговый каталог</label>
	</div>
	<div><h4 class="bg-primary"> Скидки в Интернет магазине применяются к </h4>

		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> товарам.</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> к суммам заказов в определенный период времени.</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> к товарам по акции.</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> к суммам заказов.</label>
	</div>
	<div><h4 class="bg-primary"> Планы аффилиатов делятся по: </h4>

		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> сумме продаж</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> количеству аффилиатов</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сумме аффилиатов</label>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> количеству продаж</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сумме планов</label>
		<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> количеству планов</label>
	</div>
</div>
