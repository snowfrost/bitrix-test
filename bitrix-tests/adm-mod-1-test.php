<h1>Администратор. Модули. Коллективная работа.</h1><div>
	<div>
		<h4 class="bg-primary">Для страницы сайта заданы следующие обязательные (required) ключевые слова: поддержка, support, консультация. Какой (какие) из перечисленных ниже баннеров будет доступен для показа на данной странице?</h4>
		<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Banner3. Ключевые слова: не заданы</label>
	</div>
<div><h4 class="bg-primary">Если посетитель пришел на сайт с параметрами referer1 и/или referer2, и с такими идентификаторами не было найдено ни одной рекламной кампании, то:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> посетитель не учтется в статистике</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> автоматически будет создана новая рекламная кампания</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> посетитель учтеться в последней добавленной рекламной компании</label>
</div><div><h4 class="bg-primary">Расставьте параметры обращения в порядке уменьшения приоритета при автоматическом назначении ответственного сотрудника:</h4>
<label class=""><input type="checkbox" class="" value=""/> 1. SLA</label>
<label class=""><input type="checkbox" class="" value=""/> 2. Настройки категории, к которой было отнесено обращение</label>
<label class=""><input type="checkbox" class="" value=""/> 3. Настройки модуля техподдержки</label>
</div><div><h4 class="bg-primary">Укажите ошибочные действия для организации документооборота</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настроить права данной группы пользователей на статусы документов</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> разрешить группе пользователей просмотр истории документов</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> установить для группы право доступа к модулю "Управление структурой" равным "Редактирование файлов и папок"</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> разрешить доступ группе пользователей к Менеджеру файлов в настройках "Главного модуля"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в Менеджере файлов для папок и файлов для группы установить право доступа "Документооборот"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> добавить право доступа "Участие в документообороте" для группы, к которой относится сотрудник в настройках модуля</label>
</div><div><h4 class="bg-primary">Социальные сервисы настраиваются</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> как для всех сайтов сразу, так и для каждого сайта по отдельности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для каждого сайта в отдельности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для всех сайтов системы</label>
</div><div><h4 class="bg-primary">Фильтрация посетителей по записям Стоп-листа будет производиться только:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при совпадении всех параметров;</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> при совпадении одного любого параметра.</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> при совпадении обязательных параметров;</label>
</div><div><h4 class="bg-primary">Список попыток прохождения теста по итогам курса фиксируется на странице:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Журнал</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Попытки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Тесты</label>
</div><div><h4 class="bg-primary">На странице Сертификация можно:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> удалить сертификат</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> создать новый сертификат</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> все из перечисленного</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> изменить данные сертификата</label>
</div><div><h4 class="bg-primary">Инфоблоки могут принимать участие в:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> и в бизнес-процессах, и в документообороте</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> либо в бизнес-процессах, либо в документообороте</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только в документообороте</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только в бизнес-процессах</label>
</div><div><h4 class="bg-primary">На странице Журнал фиксируется:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> результат последней попытки тестирования для каждого пользователя</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> результат наиболее успешной попытки прохождения теста для каждого пользователя</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> количество попыток прохождения теста каждым пользователем</label>
</div><div><h4 class="bg-primary">Использование Универсальных списков в Социальной сети:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> включается дополнительно в настройках модуля Универсальные списки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> включается дополнительно в настройках модуля Социальная сеть</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> включено по умолчанию</label>
</div><div><h4 class="bg-primary">При настройке компонента "Баннер" тип выбранного баннера определяет:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> место показа баннера на странице сайта</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> вероятность показа баннера на странице сайта</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> частоту показа баннера на странице сайта</label>
</div><div><h4 class="bg-primary">Какие настройки необходимо выполнить для получения обращений в техподдержку по почте?</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настройка главного модуля</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> настройка модуля "Техподдержка"</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> должна быть заведена учетная запись электронной почты службы техподдержки</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> создано новое правило обработки почтовых сообщений</label>
</div><div><h4 class="bg-primary">Управление функционалом социальной сети выполняется:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только для всех сайтов сразу</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> как для всех сайтов сразу, так и для каждого сайта по отдельности</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для каждого сайта в отдельности</label>
</div><div><h4 class="bg-primary">Все группы пользователей социальной сети по умолчанию имеют следующие права:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> работа в публичной части с правом создания рабочих групп</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> работа в публичной части без права создания рабочих групп</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> просмотр административной части</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> полный доступ</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> работа в административной части</label>
</div><div><h4 class="bg-primary">Каким уровнем прав на доступ к модулю «Реклама, баннеры» должен обладать пользователь для того, чтобы он мог быть приписан к рекламному контракту:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> администратор рекламы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> рекламодатель</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> менеджер баннеров</label>
</div><div><h4 class="bg-primary">Тип события определяется полями:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> event 1, event 2, event 3</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> event 1</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> event 1, event 2</label>
</div><div><h4 class="bg-primary">Чтобы рекламная кампания считала все переходы на наш сайт в поле "referer2" надо добавить:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> index</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> all</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> noindex</label>
</div><div><h4 class="bg-primary">SLA определяет:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Уровни критичности обращений</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Ответственного за обращение</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Время реакции на обращение пользователя</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Расписание техподдержки</label>
</div><div><h4 class="bg-primary">Вопросы, участвующие в тесте, формируются на основе вопросов:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> К самой главе</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Непосредственно в тесте</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> К урокам главы</label>
</div><div><h4 class="bg-primary">Какие баннеры будут показаны на странице сайта?</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Баннеры, набор ключевых слов которых включает наибольшее количество обязательных ключевых слов станицы</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> На данной странице не будет осуществляться показ баннеров</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Баннеры, для которых ключевые слова не заданы</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Все баннеры</label>
</div><div><h4 class="bg-primary">Тема групп это:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> совокупность рабочих групп, обладающих некоторой общей тематикой, признаком</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> признак, по которому сотрудники объединяются в группы</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> предмет обсуждения в форуме/блоге</label>
</div><div><h4 class="bg-primary">Просмотр всех обращений в службу техподдержки доступен:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> клиентам техподдержки</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> администратору техподдержки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сотрудникам техподдержки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> всем</label>
</div><div><h4 class="bg-primary">В модуле Документооборот:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> документы со статусом, идентификатор которого равен 1, не могут быть удалены</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> статусы позволяют настроить права доступа к нужному информационному блоку</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> статус с идентификатором равным 1 зарезервирован и не подлежит удалению</label>
</div><div><h4 class="bg-primary">Доступ к темам и сообщениям форумов социальной сети задается:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настройками доступа форумов</label>
<label class="test_item_not"><input type="checkbox" class="test_item_ok" value=""/> персональными настройками сотрудников и рабочих групп</label>
<label class="test_item_not"><input type="checkbox" class="test_item_ok" value=""/> настройками модуля Социальной сети</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> настройками модуля Форум</label>
</div><div><h4 class="bg-primary">При создании нового курса:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> порядок создания глав и уроков не имеет значения</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сначала создаются главы, затем уроки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сначала создаются уроки, а потом главы</label>
</div><div><h4 class="bg-primary">Выберите типы справочников, имеющиеся в системе:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> категории</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> сложность</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> статусы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> источники</label>
</div><div><h4 class="bg-primary">Интеграция Wiki в Социальную сеть включается и настраивается:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках групп или настройках страницы пользователя</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках комплексных компонентов Социальной сети</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля Социальная сеть</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках модуля Wiki</label>
</div><div><h4 class="bg-primary">Права на работу с конкретным списком определяются:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках самого списка</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в соответствии с настройками доступа в настройках модуля Универсальные списки</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в соответствии с правами доступа на страницу Списки</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках доступа информационного блока</label>
</div><div><h4 class="bg-primary">Какое количество баннеров из одной группы может быть показано одновременно на одной странице:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> 1</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в зависимости от выставленных администратором настроек</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> неограниченное количество</label>
</div><div><h4 class="bg-primary">Тип прохождения итогового теста курса определяет:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> порядок отбора уроков, вопросы которых будут включены в тест</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> порядок задания параметров отбора вопросов, отображаемых в списке</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> порядок перехода к следующему вопросу теста</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> порядок расстановки вопросов и ответов теста</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> право на изменение ответов теста</label>
</div><div><h4 class="bg-primary">Настройка времени показа баннера осуществляется путем соответствующей настройки:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> группы баннера</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> рекламного контракта баннера</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> статуса баннера</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> типа баннера</label>
</div></div>
