<h1>Администратор. Модули. Модули для общения и обратной связи</h1><div>
<div><h4 class="bg-primary">Доступ к фотогалерее определяется: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля Социальная сеть</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в разделе Управление структурой</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках доступа выбранного инфоблока</label>
</div><div><h4 class="bg-primary">Чтобы вывести закрытую рассылку в форме подписки, опубликованной с помощью компонента «Форма подписки», нужно: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> сделать закрытую рассылку активной</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> закрытые рассылки недоступны для показа в публичном разделе</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> выполнить соответствующую настройку компонента «Форма подписки» </label>
</div><div><h4 class="bg-primary">Кнопка «Добавить выпуск», расположенная на странице проверки шаблона автоматической рассылки, позволяет: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> перейти к созданию нового выпуска для отправки в рамках автоматической рассылки</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> добавить сгенерированный выпуск в список на странице «Выпуски» для последующей отправки вручную</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> подтвердить возможность отправки выпусков рассылки с использованием данного шаблона</label>
</div><div><h4 class="bg-primary">Использование статусов веб-форм позволяет: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> оценить динамику заполнения веб-форм</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> организовать поэтапную работу по созданию веб-форм</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> организовать дополнительное распределение прав доступа к результатам веб-форм</label>
</div><div><h4 class="bg-primary">Опция «Отправлять письмо персонально каждому получателю» позволяет: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> получать уведомление о прочтении сообщения получателем</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> отправлять сообщения с указанием в поле КОМУ адреса отдельного получателя</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> отправлять сообщения с указанием имени получателя в тексте письма</label>
</div><div><h4 class="bg-primary">Красный индикатор у опроса в списке опросов может означать что: </h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> текущая дата не попадает в интервал проведения опроса</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> флаг активности установлен</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> текущая дата попадает в интервал проведения опроса</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> флаг активности опроса не установлен</label>
</div><div><h4 class="bg-primary">Какое количество блогов может завести каждый отдельный пользователь? </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> неограниченное количество</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в зависимости от выставленных администратором настроек</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> один</label>
</div><div><h4 class="bg-primary">Настройка прав доступа к сообщениям и комментариям блога выполняется: </h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при создании/редактировании блога в публичном разделе</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при создании/редактировании сообщения</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> при создании/редактировании блога в административном разделе</label>
</div><div><h4 class="bg-primary">Какие утверждения правильны?</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Альбомы и фотографии галерей пользователей хранятся как разделы и элементы инфоблока</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Управление доступом к личным фотогалереям выполняется только администратором системы</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Работа модуля Фотогалерея 2.0 основана на модуле Управление структурой</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Работа модуля Фотогалерея 2.0 основана на модуле Информационные блоки</label>
</div><div><h4 class="bg-primary">Почему не рекомендуется переключаться из расширенного режима в упрощенный? </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Функционал создания вопросов в упрощенном режиме ограничен.</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> Данные, сохраненные в расширенном режиме, при переключении поменяют структуру.</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Потеряются созданные в расширенном режиме статусы.</label>
</div><div><h4 class="bg-primary">Каждый форум может: </h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> быть привязан к одной из групп форумов</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> быть привязан к нескольким группам форумов</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> остаться без привязки к какой-либо группе</label>
</div><div><h4 class="bg-primary">Подписка на рассылки сайта доступна: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только зарегистрированным пользователям сайта</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только авторизованным посетителям сайта</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> как зарегистрированным, так и незарегистрированным пользователям</label>
</div><div><h4 class="bg-primary">При работе с веб-формой в расширенном режиме необходимо: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> создать хотя бы один результат веб-формы</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> создать хотя бы одно поле веб-формы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> настроить хотя бы один статус веб-формы</label>
</div><div><h4 class="bg-primary">Привязка блога к одной из групп является: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> рекомендуемой</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> обязательной</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> необязательной</label>
</div><div><h4 class="bg-primary">Переключение между режимами редактирования веб-форм осуществляется: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на странице со списком веб-форм</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> на странице редактирования непосредственно самой веб-формы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> на странице настроек модуля «Веб-формы»</label>
</div><div><h4 class="bg-primary">Модерировать сервис "Есть идея?" могут: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только администратор системы</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> пользователи, указанные в настройках комплексного компонента идей</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> пользователи, указанные в настройках модуля</label>
</div><div><h4 class="bg-primary">Неактивные форумы доступны: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> всем пользователям</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> пользователям, которые имеют права на полный доступ к ним</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только администратору</label>
</div><div><h4 class="bg-primary">В каком случае в настройках компонента следует указывать ID опроса? </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> если опрос не входит в какую-либо группу</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> когда необходимо указать приоритет опроса</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> если в выбранной группе больше одного опроса</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> когда у опроса не выставлен флаг активности</label>
<label class=""><input type="checkbox" class="" value=""/>Как можно указать несколько e-mail для доставки почтового сообщения?    перечислить их через точку с запятой в соответствующем поле  перечислить их через запятую в соответствующем поле  указать адреса в настройках модуля Почта  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> перечислить их через точку с запятой в соответствующем поле</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> перечислить их через запятую в соответствующем поле</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> указать адреса в настройках модуля Почта</label>
<label class=""><input type="checkbox" class="" value=""/>Адреса получателя и отправителя по умолчанию, подставляемые по умолчанию в поля ОТ и КОМУ сообщения (выпуска), берутся:    из настроек модуля «Почта»  из настроек рассылки, подписчики которой получат данное сообщение  из настроек модуля «Подписка, рассылки»  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> из настроек модуля «Почта»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> из настроек рассылки, подписчики которой получат данное сообщение</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> из настроек модуля «Подписка, рассылки»</label>
<label class=""><input type="checkbox" class="" value=""/>Выпуск не может быть открыт для редактирования, если он находится в статусе:   Отправлен с ошибками  Черновик  Отправлен  Остановлен  В процессе  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Отправлен с ошибками</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Черновик</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Отправлен</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Остановлен</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> В процессе</label>
<label class=""><input type="checkbox" class="" value=""/>Чтобы результаты какого-либо голосования не учитывались при построении результирующей диаграммы, нужно:    исключить данный результат при построении диаграммы с помощью полей фильтра  пометить данный результат в настройках компонента как игнорируемый  снять флаг валидности для данного результата  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> исключить данный результат при построении диаграммы с помощью полей фильтра</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> пометить данный результат в настройках компонента как игнорируемый</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> снять флаг валидности для данного результата</label>
<label class=""><input type="checkbox" class="" value=""/>Администратор вправе назначить следующие права доступа к группе опросов для разных групп пользователей:   позволить голосовать и просматривать результаты  запретить все действия  позволить принять участие в опросах  запретить голосование, но разрешить просмотр результатов  </label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> позволить голосовать и просматривать результаты</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> запретить все действия</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> позволить принять участие в опросах</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> запретить голосование, но разрешить просмотр результатов</label>
<label class=""><input type="checkbox" class="" value=""/>Создание новых форумов выполняется:    в административной части  в публичной части  в публичной и административной части  </label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в административной части</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в публичной части</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в публичной и административной части</label>
<label class=""><input type="checkbox" class="" value=""/>Чтобы отправить сообщение ручной рассылки в указанное время, нужно на странице создания выпуска:   установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Отправить»  установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Сохранить»  указать в поле «Дата и время отправки» (закладка «Параметры») время отправки и нажать кнопку «Отправить»  </label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Отправить»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Сохранить»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> указать в поле «Дата и время отправки» (закладка «Параметры») время отправки и нажать кнопку «Отправить»</label>
<label class=""><input type="checkbox" class="" value=""/>Использование HTML в блогах возможно:    если в настройках необходимого блога включена такая опция  если это разрешено настройками модуля «Блоги»  в любом случае, вне зависимости от настроек  нельзя использовать HTML в блогах  </label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> если в настройках необходимого блога включена такая опция</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> если это разрешено настройками модуля «Блоги»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в любом случае, вне зависимости от настроек</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> нельзя использовать HTML в блогах</label>
<label class=""><input type="checkbox" class="" value=""/>Создание новых статусов Менеджера идей производится    в настройках компонента "bitrix:idea"  в административном разделе на странице "Пользовательские поля"  в административном разделе на странице настроек модуля  прямо в публичной части системы  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках компонента "bitrix:idea"</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в административном разделе на странице "Пользовательские поля"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в административном разделе на странице настроек модуля</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> прямо в публичной части системы</label>
<label class=""><input type="checkbox" class="" value=""/>Где можно выбрать шаблон для отображения страниц блогов?    в настройках страниц  в настройках компонента  в настройках блога  в настройках модуля блогов  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках страниц</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках компонента</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках блога</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля блогов</label>
<label class=""><input type="checkbox" class="" value=""/>Управлять определенным функционалом фотогалерей в модуле Социальной сети можно:    для всех сайтов сразу и для конкретного сайта отдельно  только раздельно по каждому сайту  только для всех сайтов сразу  </label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для всех сайтов сразу и для конкретного сайта отдельно</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только раздельно по каждому сайту</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только для всех сайтов сразу</label>
</div><div><h4 class="bg-primary">Как можно указать несколько e-mail для доставки почтового сообщения? </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> перечислить их через точку с запятой в соответствующем поле</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> перечислить их через запятую в соответствующем поле</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> указать адреса в настройках модуля Почта</label>
</div><div><h4 class="bg-primary">Адреса получателя и отправителя по умолчанию, подставляемые по умолчанию в поля ОТ и КОМУ сообщения (выпуска), берутся: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> из настроек модуля «Почта»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> из настроек рассылки, подписчики которой получат данное сообщение</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> из настроек модуля «Подписка, рассылки»</label>
</div><div><h4 class="bg-primary">Выпуск не может быть открыт для редактирования, если он находится в статусе:</h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Отправлен с ошибками</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Черновик</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Отправлен</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> Остановлен</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> В процессе</label>
</div><div><h4 class="bg-primary">Чтобы результаты какого-либо голосования не учитывались при построении результирующей диаграммы, нужно: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> исключить данный результат при построении диаграммы с помощью полей фильтра</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> пометить данный результат в настройках компонента как игнорируемый</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> снять флаг валидности для данного результата</label>
</div><div><h4 class="bg-primary">Администратор вправе назначить следующие права доступа к группе опросов для разных групп пользователей:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> позволить голосовать и просматривать результаты</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> запретить все действия</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> позволить принять участие в опросах</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> запретить голосование, но разрешить просмотр результатов</label>
</div><div><h4 class="bg-primary">Создание новых форумов выполняется: </h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в административной части</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в публичной части</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в публичной и административной части</label>
</div><div><h4 class="bg-primary">Чтобы отправить сообщение ручной рассылки в указанное время, нужно на странице создания выпуска:</h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Отправить»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> установить флажок в поле «Отправить автоматически в указанное время» (закладка «Параметры»), указать в поле «Дата и время отправки» время отправки и нажать кнопку «Сохранить»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> указать в поле «Дата и время отправки» (закладка «Параметры») время отправки и нажать кнопку «Отправить»</label>
</div><div><h4 class="bg-primary">Использование HTML в блогах возможно: </h4>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> если в настройках необходимого блога включена такая опция</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> если это разрешено настройками модуля «Блоги»</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в любом случае, вне зависимости от настроек</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> нельзя использовать HTML в блогах</label>
</div><div><h4 class="bg-primary">Создание новых статусов Менеджера идей производится </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках компонента "bitrix:idea"</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в административном разделе на странице "Пользовательские поля"</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в административном разделе на странице настроек модуля</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> прямо в публичной части системы</label>
</div><div><h4 class="bg-primary">Где можно выбрать шаблон для отображения страниц блогов? </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках страниц</label>
<label class="test_item_ok"><input type="checkbox" class="test_item_ok" value=""/> в настройках компонента</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках блога</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> в настройках модуля блогов</label>
</div><div><h4 class="bg-primary">Управлять определенным функционалом фотогалерей в модуле Социальной сети можно: </h4>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> для всех сайтов сразу и для конкретного сайта отдельно</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только раздельно по каждому сайту</label>
<label class="test_item_not"><input type="checkbox" class="test_item_not" value=""/> только для всех сайтов сразу</label>
</div></div>
