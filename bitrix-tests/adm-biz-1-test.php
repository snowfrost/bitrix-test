<h1>Администратор. Бизнес. Валюты и Торговый каталог</h1>
<div>
	<div>
		<h4 class="bg-primary">Базовая валюта - это валюта,</h4>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> относительно которой рассчитываются другие валюты</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> которая обязательно используется в информере курсов валют</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> которая обязательно должна присутствовать в системе</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> номинал которой равен ее курсу.</label>
	</div>
	<div><h4 class="bg-primary">Форма редактирования товара торгового каталога позволяет настроить:</h4>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> значения типов цен для товара</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> ставку НДС</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> вес единицы товара</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> параметры доставки товара заказчику</label>
	</div>
	<div><h4 class="bg-primary">Информер курса валют позволяет:</h4>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> вывести курс валют с ЦБ РФ</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> вывести курс валют сайта</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> изменить базовую валюту</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> создавать курсы валют на сайте</label>
	</div>
	<div><h4 class="bg-primary">Если в форме настройки валюты нет ее представления на нужном вам языке, значит:</h4>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> в системе не добавлен данный язык</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> не настроено отображение валюты на данном языке</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> не указана базовая валюта</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> валюта не имеет формы представления на данном языке</label>
	</div>
	<div><h4 class="bg-primary">На работу в режиме торгового каталога могут быть настроены: </h4>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> любые информационные блоки</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> инфоблоки, имеющие иерархическую структуру (т.е. содержащие разделы и подразделы)</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> инфоблоки, типу которых присвоен идентификатор (ID) "catalog"</label>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> инфоблоки типа "Каталог"</label>
	</div>
	<div><h4 class="bg-primary">Настройку инфоблока на работу в режиме торгового каталога можно выполнить: </h4>
		<label class="test_item_not"><input type="checkbox"
				class="test_item_not"
				value=""/> на странице настроек модуля "Информационные блоки"</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> на странице настроек модуля "Торговый каталог"</label>
		<label class="test_item_ok"><input type="checkbox"
				class="test_item_ok"
				value=""/> в форме настройки параметров информационного блока (отдельно для каждого инфоблока)</label>
	</div>
</div>
